using UnityEngine;
using System.Collections;

public class Test3DMapping : FullScreenMovie 
{
	private float _position;

	public new void Start()
	{
		base.Start();
	}
	
	public new void Update()
	{
		base.Update();
	}
	
	public new void OnGUI()
	{
		
		if(!bIsDonePlaying && _moviePlayer.IsDonePlaying()){
			bIsDonePlaying = true;
			if(LoadSceneOnDone != null)
				Application.LoadLevel(LoadSceneOnDone);
		}
		
		if(Input.GetKeyDown(KeyCode.Escape) && LoadSceneOnDone != null && AllowSkip)
			Application.LoadLevel(LoadSceneOnDone);
	}
}
