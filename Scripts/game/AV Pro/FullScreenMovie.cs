using UnityEngine;
using System.Collections;

[AddComponentMenu("AV Pro Windows Media/FullScreenMovie")]
public class FullScreenMovie : MoviePlayer
{
	public ScaleMode _scaleMode = ScaleMode.ScaleToFit;
	public bool _alphaBlend = false;
	public int  _depth = 0;

	public string LoadSceneOnDone;
	public bool AllowSkip = false;
	protected bool bIsDonePlaying = false;
	
	//-------------------------------------------------------------------------

	public void OnGUI()
	{
		if (_moviePlayer != null && _moviePlayer.OutputTexture != null)
		{
			int prevDepth = GUI.depth;
			
			GUI.depth = _depth;
			GUI.DrawTexture(new Rect(0.0f, 0.0f, Screen.width, Screen.height), _moviePlayer.OutputTexture, _scaleMode, _alphaBlend);
			
			GUI.depth = prevDepth;

			if(!bIsDonePlaying && _moviePlayer.IsDonePlaying()){
				bIsDonePlaying = true;
				if(LoadSceneOnDone != null)
					Application.LoadLevel(LoadSceneOnDone);
			}
		}
		if(Input.GetKeyDown(KeyCode.Escape) && LoadSceneOnDone != null && AllowSkip)
			Application.LoadLevel(LoadSceneOnDone);
	}	


}