using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FruitShooter : MonoBehaviour
{
	public Rigidbody[] Fruits;
	public Rigidbody[] BonusFruit;
	public float throwArch;
	public float throwSpeed;
	public Vector3 throwDiff = Vector3.zero;

	private Vector3 playerPos;
	private float nextFire = 0.0f;
	private float nextFruitFire = 0.0f;
	private float bonusFruitFire = 0.0f;
	private int amountToShoot = 1;
	private bool bonusShot = false;
	//private int fruitsShot = 0;

	void Start ()
	{
		playerPos = GameObject.Find("_cameras").transform.position;
	}

	void Update ()
	{	
		if(GameManager.Instance.gameState == GameStates.CountDown)
		{
			bonusFruitFire = 0.0f;
			bonusShot = false;
		}
		if (GameManager.Instance.gameState == GameStates.Playing && (Time.time > nextFire || amountToShoot > 1))
		{
			if(GameManager.Instance.timeLeft > 4)
			{
				if(amountToShoot < 1)
				{
					nextFire = Time.time + Random.Range(0.5f,4.0f);
					amountToShoot = Random.Range(1, 5);
				}

				if(Time.time > nextFruitFire)
				{
					nextFruitFire = Time.time + Random.Range(0.1f,0.3f);
					amountToShoot--;

					Rigidbody fruitClone;
					int fruitNum = Random.Range(0, Fruits.Length);
					
					Vector3 rndThrow = new Vector3(Random.Range(-throwDiff.x,throwDiff.x), Random.Range(throwDiff.y,throwDiff.y*2), Random.Range(throwDiff.z-(throwDiff.z*2),throwDiff.z));
					Vector3 rndThrowPos = new Vector3(transform.position.x+rndThrow.x,transform.position.y+rndThrow.y, transform.position.z+rndThrow.z);
					Vector3 targetDir = playerPos - rndThrowPos;
					targetDir.y = throwArch + Random.Range(-0.3f, 0.3f);
					
					fruitClone = Instantiate(Fruits[fruitNum],rndThrowPos, transform.rotation) as Rigidbody;
					fruitClone.velocity = targetDir * throwSpeed;
					fruitClone.AddTorque(new Vector3(Random.Range(-41, 60),Random.Range(-41, 60),Random.Range(-41, 60)));
					fruitClone.audio.Play();
					
					GameObject.Destroy(fruitClone.gameObject, 6.0f);
					//fruitsShot++;
					

					ScoreKeeper.Instance.fruitsShot = ScoreKeeper.Instance.fruitsShot + 1;
				}

				if(bonusFruitFire == 0.0f)
					bonusFruitFire = Time.time + Random.Range(5f,40f);

				if(Time.time > bonusFruitFire && !bonusShot)
				{
					bonusShot = true;
					Rigidbody fruitClone;
					int fruitNum = Random.Range(0, BonusFruit.Length-1);
					
					Vector3 rndThrow = new Vector3(Random.Range(-throwDiff.x,throwDiff.x), Random.Range(throwDiff.y,throwDiff.y*2), Random.Range(throwDiff.z-(throwDiff.z*2),throwDiff.z));
					Vector3 rndThrowPos = new Vector3(transform.position.x+rndThrow.x,transform.position.y+rndThrow.y, transform.position.z+rndThrow.z);
					Vector3 targetDir = playerPos - rndThrowPos;
					targetDir.y = throwArch + Random.Range(-0.3f, 0.3f);
					
					fruitClone = Instantiate(BonusFruit[fruitNum],rndThrowPos, transform.rotation) as Rigidbody;
					fruitClone.velocity = targetDir * throwSpeed;
					fruitClone.AddTorque(new Vector3(Random.Range(-41, 60),Random.Range(-41, 60),Random.Range(-41, 60)));
					fruitClone.audio.Play();
					
					GameObject.Destroy(fruitClone.gameObject, 6.0f);
				}

//				for(int i = 0; i <= Random.Range(1,5); i++){
//					Rigidbody fruitClone;
//					int fruitNum = Random.Range(0, Fruits.Length);
//
//					Vector3 rndThrow = new Vector3(Random.Range(-throwDiff.x,throwDiff.x), Random.Range(throwDiff.y,throwDiff.y*2), Random.Range(throwDiff.z-(throwDiff.z*2),throwDiff.z));
//					Vector3 rndThrowPos = new Vector3(transform.position.x+rndThrow.x,transform.position.y+rndThrow.y, transform.position.z+rndThrow.z);
//					Vector3 targetDir = playerPos - rndThrowPos;
//					targetDir.y = throwArch;
//
//					fruitClone = Instantiate(Fruits[fruitNum],rndThrowPos, transform.rotation) as Rigidbody;
//					fruitClone.velocity = targetDir * throwSpeed;
//					fruitClone.AddTorque(new Vector3(Random.Range(-41, 60),Random.Range(-41, 60),Random.Range(-41, 60)));
//
//					GameObject.Destroy(fruitClone.gameObject, 6.0f);
//					fruitsShot++;
//				}

				GameManager.Instance.updateGUI("GUI_Stats", ScoreKeeper.Instance.fruitsShot.ToString());
			}
	    }
	}
}
