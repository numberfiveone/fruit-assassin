﻿using UnityEngine;
using System.Collections;

public class FadeBlack : MonoBehaviour {

	public Texture2D fadeOutTexture;
	public float fadeSpeed = 2.0f;
	
	public int drawDepth = -1000;
	
	public bool alphaWait = true;
	
	
	private float alpha = 1.0f;
	private float fadeDir = -1;
	
	//--------------------------------------------------------------------
	//                       Runtime functions
	//--------------------------------------------------------------------
	
	//--------------------------------------------------------------------
	
	void OnGUI(){
		
		if(alphaWait == false) {
			
			alpha += fadeDir * fadeSpeed * Time.deltaTime;
		}
		
		alpha = Mathf.Clamp01(alpha);  
		var tempColor = GUI.color;
		tempColor.a = alpha;

		GUI.color = tempColor;
		
		GUI.depth = drawDepth;
		
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), fadeOutTexture);
		
	}
	
	//--------------------------------------------------------------------
	
	IEnumerator fadeIn(){
		
		yield return new WaitForSeconds(1);
		alphaWait = false;
		
		fadeDir = -1;  
	}
	
	//--------------------------------------------------------------------
	
	void fadeOut(){
		fadeDir = 1;  
		
		
	}
	
	void Start(){
		alpha=1;
		StartCoroutine(fadeIn());
	}

//	public float width = 100;
//	public float height = 100;
//	
//	public Color background = new Color(0.6f, 1f, 0.2f, 1f);
//	private GameObject plane;
//
//	void Start () {
//		plane = new GameObject("fader");
//		
//		MeshFilter meshFilter = (MeshFilter)plane.AddComponent(typeof(MeshFilter));
//		meshFilter.mesh = CreateMesh(width, height);
//		MeshRenderer renderer = plane.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
//		renderer.material.shader = Shader.Find ("Transparent/Diffuse" );
//		Texture2D tex = new Texture2D(1, 1);
//		tex.SetPixel(0, 0, background);
//		tex.Apply();
//		renderer.material.mainTexture = tex;
//		renderer.material.color = background;
//		
//		plane.transform.parent = transform;
//		plane.transform.position = transform.position;
//		plane.transform.localRotation = new Quaternion(0,180,0,0);
//		plane.transform.localPosition = new Vector3(0, 0, 0.15f);
//		plane.transform.localScale = new Vector3 (1f,1f,1f);
//	}
//	
//	// Update is called once per frame
//	void Update () {
//		
//	}
//
//	private Mesh CreateMesh(float width, float height)
//	{
//		Mesh m = new Mesh();
//		m.name = "ScriptedMesh";
//		m.vertices = new Vector3[] {
//			new Vector3(-width, -height, 0.01f),
//			new Vector3(width, -height, 0.01f),
//			new Vector3(width, height, 0.01f),
//			new Vector3(-width, height, 0.01f)
//		};
//		m.uv = new Vector2[] {
//			new Vector2 (0, 0),
//			new Vector2 (0, 1),
//			new Vector2(1, 1),
//			new Vector2 (1, 0)
//		};
//		m.triangles = new int[] { 0, 1, 2, 0, 2, 3};
//		m.RecalculateNormals();
//		
//		return m;
//	}
}
