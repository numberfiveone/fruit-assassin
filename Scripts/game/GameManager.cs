using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum GameStates {
	Startup,
	Waiting,
	Menu,
	Calibrate,
	Recalibrate,
	Ready,
	CountDown,
	Paused,
	Unpaused,
	Playing,
	GameOver
}

public class GameManager : MonoBehaviour {
	// Singleton  
	private  static GameManager instance;   
	
	// Construct  
	//private Clicker() {}    
	
	//  Instance  
	public static GameManager Instance  
	{     
		get     
		{       
			if (instance ==  null)
				instance = GameObject.FindObjectOfType(typeof(GameManager)) as  GameManager;      
			return instance;    
		}   
	}
	
	public GameObject Menu;
	public GameObject[] Panels;
	public GameStates StartingGameState = GameStates.Startup;
	public bool HideCursorOnStart = false;

	[HideInInspector]
	public GameStates gameState;

	[HideInInspector]
	public GameObject _GUI_Timer;
	[HideInInspector]
	public GameObject _GUI_Calibrate;
	[HideInInspector]
	public GameObject _GUI_Countdown;

	[HideInInspector]
	public TextMesh GUI_Timer;
	[HideInInspector]
	public TextMesh GUI_Calibrate;
	[HideInInspector]
	public TextMesh GUI_Countdown;

	[HideInInspector]
	public bool bRHandStatus = false;
	[HideInInspector]
	public bool bLHandStatus = false;
	
	[HideInInspector]
	public float timeLeft = 60;
	[HideInInspector]
	public float countdown = 5.0f;

	
	[HideInInspector]
	public List<string> currentBonuses;
	[HideInInspector]
	public Dictionary<string, string> runningBonuses;
	[HideInInspector]
	public Dictionary<string, float> runningBonusTime;

	[HideInInspector]
	public bool bonus;
	[HideInInspector]
	public float bonusTime;

	public GameObject[] weaps;

	// Use this for initialization
	protected virtual void Start ()
	{
		HideCursor (HideCursorOnStart);
		gameState = StartingGameState;
		runningBonuses = new Dictionary<string, string>();
		runningBonusTime = new Dictionary<string, float>();

		_GUI_Timer = GameObject.Find("GUI_Timer");
		_GUI_Calibrate = GameObject.Find("GUI_Calibrate");
		_GUI_Countdown = GameObject.Find("GUI_Countdown");

		if(_GUI_Timer != null)
			GUI_Timer = _GUI_Timer.GetComponent<TextMesh>();

		if(_GUI_Calibrate != null)
			GUI_Calibrate = _GUI_Calibrate.GetComponent<TextMesh>();

		if(_GUI_Countdown != null)
			GUI_Countdown = _GUI_Countdown.GetComponent<TextMesh>();

		StartLevel();
	}

	public void StartLevel()
	{
		Time.timeScale = 1;
		timeLeft = 60;
		countdown = 5.0f;
		bonus = false;

		if(ScoreKeeper.Instance != null)
			ScoreKeeper.Instance.StartLevel();

		if(GUI_Calibrate != null)
			GUI_Calibrate.text = "5";
		
		if(GUI_Timer != null)
			GUI_Timer.text = "GET READY!";

	}
	void DoBonus(string Bonus)
	{
		if(Bonus == "flame")
		{
			if(runningBonuses.ContainsKey(Bonus) && runningBonuses[Bonus] == "running"  )
			{
				if(Time.time <= runningBonusTime[Bonus])
					return;

				for(int i = 0; i<weaps.Length; i++)
				{
					StartCoroutine(weaps[i].gameObject.GetComponent<weapon>().StopBonus(Bonus));
				}

				runningBonuses[Bonus] = "";
				currentBonuses.Remove(Bonus);
				bonus = false;
			}
			else
			{
				if(!runningBonuses.ContainsKey(Bonus))
					runningBonuses.Add(Bonus, "running");
				else
					runningBonuses[Bonus] = "running";
				
				if(!runningBonusTime.ContainsKey(Bonus))
					runningBonusTime.Add(Bonus, Time.time + 20);
				else
					runningBonusTime[Bonus] = Time.time + 20;
				
				for(int i = 0; i<weaps.Length; i++){
					weaps[i].gameObject.GetComponent<weapon>().DoBonus(Bonus);
				}
			}
		}
	}

	void Update ()
	{
		if(bonus)
		{
			for(int i = 0; i<currentBonuses.Count; i++)
			{
				DoBonus(currentBonuses[i]);
			}
		}

		switch(gameState)
		{
			/*case GameStates.Startup:

			break;
			case GameStates.Menu:

			break;*/
			case GameStates.Calibrate:
				GameStateCalibrate();
			break;
			case GameStates.Recalibrate:
				GameStateRecalibrate();
			break;
			case GameStates.Ready:
				GUI_Calibrate.text = "PRESS BUTTON 1 TO START";
				if(!bRHandStatus || !bLHandStatus)
					gameState = GameStates.Calibrate;
			break;
			case GameStates.CountDown:
				GameStateCountDown();
			break;
			case GameStates.Paused:
				GUI_Calibrate.text = "GAME PAUSED";
				Menu.SetActive(true);
				Panels[0].SetActive(false);
				Panels[1].SetActive(true);
                Time.timeScale = 0;
			break;
			case GameStates.Unpaused:
				GUI_Calibrate.text = "";
                Time.timeScale = 1;
                gameState = GameStates.Playing;
			break;
			case GameStates.Playing:
				GameStatePlaying();
			break;
			case GameStates.GameOver:
				Menu.SetActive(true);
				Panels[0].SetActive(true);
				Panels[1].SetActive(false);
			break;
		}
	}

	public void GameStateCountDown()
	{
		GUI_Calibrate.text = "";
		if (countdown <= 0.0f)
		{
			//GUI_Countdown.text = "";
			GUI_Calibrate.text = "";
			
			//if(!gameObject.audio.isPlaying)
			//  gameObject.audio.Play();
			
			gameState = GameStates.Playing;
		}
		else if(countdown > 0.0f)
		{
			countdown -= Time.deltaTime;
			string seconds = (countdown % 60).ToString("0");
			//GUI_Countdown.text = string.Format("{0}", seconds);
			GUI_Calibrate.text = string.Format("{0}", seconds);
		}
	}

	public void GameStateCalibrate()
    {
		//GUI_Countdown.text = "";
        if(!bRHandStatus && !bLHandStatus)
        {
			GUI_Calibrate.text = "Hold both fists to your heart and pull triggers";
        }
        else if(!bRHandStatus)
        {
			GUI_Calibrate.text = "Hold right fist to your heart and pull trigger";
        }
        else if(!bLHandStatus)
        {
			GUI_Calibrate.text = "Hold left fist to your heart and pull trigger";
        }
        else if(bRHandStatus && bLHandStatus)
        {
			gameState = GameStates.Ready;
        }
	}

	public void GameStateRecalibrate()
	{
		Menu.SetActive(true);
		Panels[0].SetActive(true);
		Panels[1].SetActive(false);
		//GUI_Countdown.text = "";
		GUI_Calibrate.text = "";
		if(!bRHandStatus && !bLHandStatus)
		{
			GUI_Calibrate.text = "Hold both fists to your heart and pull triggers";
		}
		else if(!bRHandStatus)
		{
			GUI_Calibrate.text = "Hold right fist to your heart and pull trigger";
		}
		else if(!bLHandStatus)
		{
			GUI_Calibrate.text = "Hold left fist to your heart and pull trigger";
		}
		else if(bRHandStatus && bLHandStatus)
		{
			gameState = GameStates.Paused;
		}

	}


	public void GameStatePlaying()
    {
		GUI_Calibrate.text = "";
		Menu.SetActive(false);

        if(timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;

            string minutes = Mathf.Floor(timeLeft / 60).ToString("0");
            string seconds = (timeLeft % 60).ToString("00");

			GUI_Timer.text = string.Format("{0}:{1}", minutes, seconds);
        }
        else if (timeLeft <= 0)
        {
			double perc = ScoreKeeper.Instance.fruitsSliced * 100.0 / ScoreKeeper.Instance.fruitsShot;
			//GUI_Countdown.text = "";


			GUI_Calibrate.text = string.Format("GAME OVER\n <size=40>YOUR SCORE: {0}</size> \n\n PLAY AGAIN?\n <size=40>PUSH BUTTON 1 TO START\n\n{1} OF {2} FRUITS SLICED\n YOU ARE {3}% ASSASSIN!</size>", ScoreKeeper.Instance.currentScore, ScoreKeeper.Instance.fruitsSliced, ScoreKeeper.Instance.fruitsShot, Math.Round(perc, 0));
    	
			/* Game over, check if there's a new high score */
			ScoreKeeper.Instance.updateEndScore();
			//gameObject.audio.Stop();


			GUI_Timer.text = "";
			gameState = GameStates.GameOver;
			
			GameObject.Find("Left Hand").GetComponent<SixenseHandController>().EnableHand();
			GameObject.Find("Right Hand").GetComponent<SixenseHandController>().EnableHand();

        }
    }

    public void LoadMainMenu ()
    {
        Application.LoadLevel("menu-test");
    }

    public void HideCursor (bool hidden = true)
    {
        if(Application.isEditor == false)
        {
	        if(hidden)
	        {
	            Screen.showCursor = false;
                Screen.lockCursor = true;
	        }
	        else
	        {
	            Screen.showCursor = true;
                Screen.lockCursor = false;
	        }
        }
    }

    public void updateGUI(string text3d, string text){
        var t = GameObject.Find(text3d);
        if(t != null){
            t.GetComponent<TextMesh>().text = text;
        }
    }
}