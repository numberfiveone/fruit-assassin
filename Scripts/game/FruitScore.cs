using UnityEngine;
using System.Collections;

public class FruitScore : AbstractSliceHandler {
	
	public int points = 25;
	public string fruit;
	public Font font;
	public Material fontMaterial;

	private Vector3 scale;
	private float nextTime;
	private Vector3 minus;
	private float awakeTime;
	
	void Awake()
	{
		awakeTime = Time.time;
		nextTime = 5.0f;
		ScoreKeeper.Instance.fruitShotAdd(fruit);
	}
	
	void Start()
	{
		scale = gameObject.transform.localScale;
		minus.x = scale.x * 10 / 10;
		minus.y = scale.y * 10 / 10;
		minus.z = scale.z * 10 / 10;
	}
	
	void Update()
	{	
		
		if ((Time.time-awakeTime) > nextTime && gameObject.transform.localScale.x > 0)
		{
			gameObject.transform.localScale -= minus * Time.deltaTime;
		}
	}
	
	public override void handleSlice( GameObject[] results )
	{
		if(GameManager.Instance.gameState == GameStates.Playing)
		{
			if(GameManager.Instance.bonus){
				if(GameManager.Instance.currentBonuses.IndexOf("flame") != -1)
				{
					points = points * 2;
				}
			}
			ScoreKeeper.Instance.updateScore(points);
			if(gameObject.name.IndexOf("(Clone)(Clone)") == -1){
				ScoreKeeper.Instance.fruitsSliced = ScoreKeeper.Instance.fruitsSliced + 1;
				ScoreKeeper.Instance.fruitSliceAdd(fruit);
			}

			GameObject pointText = new GameObject("points");
			pointText.transform.position = gameObject.transform.position + new Vector3(Random.Range(-0.5f,0.5f),Random.Range(0,0.5f),0);
			pointText.transform.localScale = new Vector3 (0.1f,0.1f,0.1f);
			TextMesh textMeshComponent = pointText.AddComponent<TextMesh>();
			pointText.AddComponent<FloatUp>();
			textMeshComponent.font = font;
			textMeshComponent.fontSize = 58;// fontSize;
			textMeshComponent.text = points.ToString();
			pointText.renderer.material = fontMaterial;

			GameObject.Destroy(pointText, 0.6f);
		}
	}
}
