﻿using UnityEngine;
using System.Collections;

public class FruitBonus : AbstractSliceHandler {

	public string Bonus;
	private Vector3 scale;
	private float nextTime;
	private Vector3 minus;
	private float awakeTime;
	
	void Awake()
	{
		awakeTime = Time.time;
		nextTime = 5.0f;
	}
	
	void Start()
	{
		scale = gameObject.transform.localScale;
		minus.x = scale.x * 10 / 10;
		minus.y = scale.y * 10 / 10;
		minus.z = scale.z * 10 / 10;
	}
	
	void Update()
	{	
		
		if ((Time.time-awakeTime) > nextTime && gameObject.transform.localScale.x > 0)
		{
			gameObject.transform.localScale -= minus * Time.deltaTime;
		}
	}
	
	public override void handleSlice( GameObject[] results )
	{
		if(GameManager.Instance.gameState == GameStates.Playing)
		{
			if(gameObject.name.IndexOf("(Clone)(Clone)") == -1){
				GameManager.Instance.currentBonuses.Add(Bonus);
				GameManager.Instance.bonus = true;
				MusicPlaylist.Instance.PlayTrack(0);
				MusicPlaylist.Instance.audio.volume = 1;
			}
		}
	}
}
