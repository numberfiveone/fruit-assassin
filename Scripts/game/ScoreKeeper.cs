using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System;
using System.IO;
using System.Text;
using SimpleJSON;
using System.Text.RegularExpressions;

public class ScoreKeeper : MonoBehaviour {
	// Singleton  
	private  static ScoreKeeper instance;   
	
	//  Instance  
	public static ScoreKeeper Instance  
	{     
		get     
		{       
			if (instance ==  null)
				instance = GameObject.FindObjectOfType(typeof(ScoreKeeper)) as  ScoreKeeper;      
			return instance;    
		}   
	}

	[NonSerialized]
	public int currentScore;
	[NonSerialized]
	public int highScore;
	[NonSerialized]
	public int highScore_w;
	[NonSerialized]
	public int fruitsShot;
	[NonSerialized]
	public int fruitsSliced;
	[NonSerialized]
	public bool newScore;

	[NonSerialized]
	public Dictionary<string, int> fruitSliced;
	[NonSerialized]
	public Dictionary<string, int> fruitShot;

	private string scene;
	private string key_highscore;
	private string nametext;

	// Use this for initialization
	public void StartLevel ()
	{
		newScore = false;
		nametext = "";
		currentScore = 0;
		scene = Application.loadedLevelName;
		key_highscore = string.Format("{0}_highscore", scene);

		GameManager.Instance.updateGUI("GUI_Score", "SCORE: 0");
		GameManager.Instance.updateGUI("GUI_Stats", "");

		StartCoroutine(getScores());
		fruitSliced = new Dictionary<string, int>();
		fruitShot = new Dictionary<string, int>();

		fruitsShot = 0;
		fruitsSliced = 0;
	}

	public void Update()
	{
		if(newScore)
		{
			int chars = 10;
			foreach (char c in Input.inputString)
			{
	            if (c == "\b"[0])
				{
	                if (nametext.Length != 0)
	                    nametext = nametext.Substring(0, nametext.Length - 1);

				}
				else
				{
	                if (c == "\n"[0] || c == "\r"[0])
					{
						Debug.Log(nametext);
	                    StartCoroutine(saveScore(nametext));
					}
	                else
					{
						if(nametext.Length >= 10)
							return;

	                    nametext += c;
					}
				}
	        }

			chars = 10 - nametext.Length;

			GameManager.Instance.updateGUI("GUI_Countdown", "");
			
			double perc = Instance.fruitsSliced * 100.0 / Instance.fruitsShot;
			GameManager.Instance.updateGUI("GUI_Calibrate", string.Format("NEW HIGH SCORE: {5}\n <size=30>ENTER YOUR NAME: {0}</size> \n <size=20>{1} CHARS - ENTER WHEN DONE</size>\n\n<size=40>{2} OF {3} FRUITS SLICED\n YOU ARE {4}% ASSASSIN!</size>", nametext, chars, Instance.fruitsSliced, Instance.fruitsShot, Math.Round(perc, 0), Instance.currentScore));

		}
	}

	public void fruitSliceAdd(string fruitName)
	{
		int sliceCount;
		if(fruitSliced.TryGetValue(fruitName, out sliceCount))
		{
			fruitSliced[fruitName] = sliceCount + 1;
		}
		else
		{
			fruitSliced[fruitName] = 1;
		}
		//Debug.LogWarning(String.Format("fruitSliceAdd: {0} {1}", fruitName, sliceCount + 1));
	}

	public void fruitShotAdd(string fruitName)
	{
		int shotCount;
		if(fruitShot.TryGetValue(fruitName, out shotCount))
		{
			fruitShot[fruitName] = shotCount + 1;
		}
		else
		{
			fruitShot[fruitName] = 1;
		}

		//Debug.LogWarning(String.Format("fruitShotAdd: {0} {1}", fruitName, shotCount + 1));
	}

	public IEnumerator getScores()
	{
		//Get scores from database
		WWW www = new WWW(string.Format("http://fruitassassin.com/score/?d=get_score&scene={0}", scene));
        yield return www;
		var worldScores = "";

		try{
			worldScores = www.text;
		}catch {
			return false;
		}

		if(!string.IsNullOrEmpty(worldScores.ToString()))
		{
			var j = JSON.Parse(worldScores);

			if((j["highscore"].ToString() == "\"null\""))
				return false;

			StringBuilder sb = new StringBuilder();

			for(var i =0;i<5;i++)
			{
				int pos = i+1;
				sb.Append(string.Format("{2}: {0} {1} \n",
					j["highscore"][i]["score"].ToString().Replace("\"",""),
					j["highscore"][i]["user"].ToString().Replace("\"",""),
					pos.ToString()));
			}

			int.TryParse(j["highscore"][4]["score"], out highScore_w);

			GameManager.Instance.updateGUI("GUI_AllTime_W", sb.ToString());

		}

		// highScore
		StringBuilder sb2 = new StringBuilder();

		for(int i = 0; i<5; i++)
		{
			string tempScoreStr = PlayerPrefs.GetString(key_highscore + i);
			int tempScore = 0;

			var s = tempScoreStr.Split(new Char [] {'_'});
			
			int.TryParse(s[0], out tempScore);

			int pos = i+1;


			try
			{
				sb2.Append(string.Format("{2}: {0} {1} \n", s[0], s[1], pos.ToString()));
			}
			catch{}


			if(i == 4)
			{
				int.TryParse(s[0], out highScore);
			}

		}
		
		GameManager.Instance.updateGUI("GUI_HighscoreLocal", sb2.ToString());
	}

	public void updateScore (int points)
	{
		if(GameManager.Instance.gameState != GameStates.Playing)
			return;

		currentScore += points;
		GameManager.Instance.updateGUI("GUI_Score", string.Format("SCORE: {0}", currentScore));
	}

	public void updateEndScore ()
	{
		newScore = false;
		GameManager.Instance.updateGUI("GUI_Score", "");
		
		if((currentScore > highScore_w && highScore_w > 0) || (currentScore > highScore))
		{
			newScore = true;
		}
		else{
			StartCoroutine(saveScore());
		}
	}

	public IEnumerator saveScore(string username = "0")
	{
		StringBuilder fruitStats = new StringBuilder();

		foreach(KeyValuePair<string, int> fruit in fruitSliced){
			fruitStats.Append(string.Format(",slice_{0}:{1}",
					fruit.Key,
					fruit.Value.ToString()));
		}

		foreach(KeyValuePair<string, int> fruit in fruitShot){
			fruitStats.Append(string.Format(",shot_{0}:{1}",
					fruit.Key,
					fruit.Value.ToString()));
		}

		// Send score data to server
		string scores = string.Format(
			"user:{10},score:{0},scene:{1},pc_name:{2},operatingSystem:{3},systemMemorySize:{4},processorCount:{5}," +
			"processorType:{6},deviceName:{7},deviceType:{8},graphicsDeviceName:{9}{11}",
			currentScore,
			scene,
			System.Environment.UserName,
			SystemInfo.operatingSystem,
			SystemInfo.systemMemorySize,
			SystemInfo.processorCount,
			SystemInfo.processorType,
			SystemInfo.deviceName,
			SystemInfo.deviceType,
			SystemInfo.graphicsDeviceName,
			username,
			fruitStats);

		if(currentScore > highScore)
		{
			bool newScoreSet = false;
			bool newScoreSet2 = false;
			for(int i = 0; i<5; i++)
			{
				string tempScoreStr = PlayerPrefs.GetString(key_highscore + i);
				int tempScore = 0;
				var s = tempScoreStr.Split(new Char [] {'_'});
				int.TryParse(s[0], out tempScore);
				if(tempScore < currentScore && !newScoreSet)
				{
					PlayerPrefs.SetString(key_highscore + i, currentScore + "_" + username);
					newScoreSet = true;
				}

				if(newScoreSet)
				{
					int ii = i+1;
					if(!newScoreSet2)
					{
						PlayerPrefs.SetString(key_highscore + ii, tempScoreStr);
						newScoreSet2 = true;
					}
					else
					{
						PlayerPrefs.SetString(key_highscore + ii, PlayerPrefs.GetString(key_highscore + ii));
					}
				}

				if(i == 4)
				{
					int.TryParse(s[0], out highScore);
				}
			}
		}


		var form = new WWWForm();
		form.AddField("d",AES_encrypt("add_score"));
		form.AddField("d1",AES_encrypt(scores));
		
		var www = new WWW("http://fruitassassin.com/score/", form);
		
		// this is what you need to add
		yield return www;
		
		double perc = Instance.fruitsSliced * 100.0 / Instance.fruitsShot;
		if(newScore)
			GameManager.Instance.updateGUI("GUI_TextInput", string.Format("SCORE: {0}", string.Format("HIGH SCORE SAVED\n <size=40>YOUR SCORE: {0}</size> \n\n PLAY AGAIN?\n <size=40>PULL TRIGGER TO START\n\n{1} OF {2} FRUITS SLICED\n YOU ARE {3}% ASSASSIN!</size>", Instance.currentScore, Instance.fruitsShot, Instance.fruitsShot, Math.Round(perc, 0))));
		
		newScore = false;
	}


	private string AES_Key = "PSVJQRk9QTEpNVU1DWUZCRVFGV1VVT0ZOV1RRU1NaWQ=";
	private string AES_IV = "YWlFLVEZZUFNaWlhPQ01ZT0lLWU5HTFJQVFNCRUJZVA=";


	private string AES_encrypt(string Input)
	{
	    var aes = new RijndaelManaged();
	    aes.KeySize = 256;
	    aes.BlockSize = 256;
	    aes.Padding = PaddingMode.PKCS7;
	    aes.Key = Convert.FromBase64String(AES_Key);
	    aes.IV = Convert.FromBase64String(AES_IV);

	    var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
	    byte[] xBuff = null;
	    using (var ms = new MemoryStream())
	    {
	        using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
	        {
	            byte[] xXml = Encoding.UTF8.GetBytes(Input);
	            cs.Write(xXml, 0, xXml.Length);
	        }

	        xBuff = ms.ToArray();
	    }

	    string Output = Convert.ToBase64String(xBuff);
	    return Output;
	}

	private string AES_decrypt(string Input)
	{
        RijndaelManaged aes = new RijndaelManaged();
        aes.KeySize = 256;
        aes.BlockSize = 256;
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;
        aes.Key = Convert.FromBase64String(AES_Key);
        aes.IV = Convert.FromBase64String(AES_IV);

        var decrypt = aes.CreateDecryptor();
        byte[] xBuff = null;
        using (var ms = new MemoryStream())
        {
            using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
            {
                byte[] xXml = Convert.FromBase64String(Input);
                cs.Write(xXml, 0, xXml.Length);
            }

            xBuff = ms.ToArray();
        }

        string Output = Encoding.UTF8.GetString(xBuff);
        return Output;
	}
}
