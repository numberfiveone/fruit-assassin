﻿using UnityEngine;
using System.Collections;

public class MoveCam : MonoBehaviour {

	public float Speed = 10.0f;
	public Vector3 StartPos;
	private Vector3 EndPos;
	

	void Start()
	{
		EndPos = transform.position;
		transform.position = StartPos;
	}
	// Update is called once per frame
	void Update () {
		if(Time.realtimeSinceStartup > 2 && Time.realtimeSinceStartup < 60){
			transform.position = Vector3.Lerp(transform.position, EndPos, Time.deltaTime * 0.2f);
		}

		//transform.Rotate(new Vector3(0,6.0f*rotationsPerMinute*Time.deltaTime,0));
	}
}
