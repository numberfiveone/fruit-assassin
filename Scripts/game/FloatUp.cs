﻿using UnityEngine;
using System.Collections;

public class FloatUp : MonoBehaviour {

	private Transform points;
	
	private Vector3 scale;
	private float nextTime;
	private Vector3 minus;
	private float awakeTime;
	
	void Start () {
		points = gameObject.transform;
	}
	
	void Update () {
		points.localPosition = points.localPosition + new Vector3(0,0.1f,0) * Time.deltaTime;
		if (points.localScale.x > 0)
		{
			points.localScale = points.localScale - new Vector3(0.2f,0.2f,0.2f) * Time.deltaTime;
		}
	}
}
