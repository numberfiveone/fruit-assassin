﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum availableMaps 
{
	DemoField,
	MountainValley
}

[System.Serializable]
public class MenuMapsList {
	public string name;
	public availableMaps sceneName;
}

public class MenuMaps : MonoBehaviour {
	public List<MenuMapsList> maps;
}
