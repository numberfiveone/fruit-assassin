﻿using UnityEngine;
using System.Collections;

public class MenuReview : MonoBehaviour {
	
	public GameObject[] texts;

	// Use this for initialization
	void OnEnable () {
		texts[0].gameObject.GetComponent<TextMesh>().text = PlayerPrefs.GetString("mapName");
		texts[1].gameObject.GetComponent<TextMesh>().text = PlayerPrefs.GetString("rightWeapon");
		texts[2].gameObject.GetComponent<TextMesh>().text = PlayerPrefs.GetString("leftWeapon");
	}
}
