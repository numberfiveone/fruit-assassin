using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;

[System.Serializable]
public class PanelItems {
	public string name;
	public KeyCode keyCode = KeyCode.None;
	public GameObject nextPanel;
	public GameObject displayModel;
	public string callFunction;
	public string optionalString;

	[HideInInspector]
	public int fontSize = 48;
	[HideInInspector]
	public float padding;
	[HideInInspector]
	public float width = 12;
	[HideInInspector]
	public Color background = new Color(0.6f, 1f, 0.2f, 1f);
	[HideInInspector]
	public Color hover = new Color(0.6f, 1f, 0.2f, 1f);
	[HideInInspector]
	public Color pressed = new Color(0.6f, 1f, 0.2f, 1f);

}


[System.Serializable]
public enum DynamicItems {
	None,
	Maps,
	LeftWeapon,
	RightWeapon,
	Review,
	Resolution,
	Quality
}

public class MenuPanel : MonoBehaviour {
	public string menuTitle;
	//public GameObject Cameras;
	public GameObject nextPanel;
	public GameObject prevPanel;
	public int fontSize = 52;
	public int padding = 0;
	public float buttonWidth = 0;
	public bool scrolling = false;
	public List<PanelItems> items;
	public List<PanelItems> dynamicItemList;
	public DynamicItems dynamicItems;
	public Color background = new Color(0.6f, 1f, 0.2f, 1f);
	public Color hover = new Color(0.6f, 1f, 0.2f, 1f);
	public Color pressed = new Color(0.6f, 1f, 0.2f, 1f);
	public Font font;
	public Material fontMaterial;

	public bool showPanelBackground = false;
	public Color panelBackground = new Color(0f, 0f, 0f, 0.3f);
	public Vector2 backgroundSize = new Vector2(100,100);

	[HideInInspector]
	public int buttonFocus;

	[HideInInspector]
	public List<GameObject> buttonList;

	//private GameObject thisObj;
	private Transform thisTransform;
	private Vector3 startPos;

	// Use this for initialization
	void Start ()
	{
		//thisObj = gameObject;
		thisTransform = transform;
		startPos = thisTransform.localPosition;

		switch(dynamicItems)
		{
			case DynamicItems.Maps:
				var listOfMaps = GetComponent<MenuMaps>();
				foreach(MenuMapsList map in listOfMaps.maps)
				{
					dynamicItemList.Add(new PanelItems() {
						name = map.name,
						nextPanel = nextPanel,
						optionalString = map.sceneName.ToString(),
						callFunction = "selectMap"
					});
				}

			break;
			case DynamicItems.RightWeapon:
				var listOfWeaponsR = GameObject.Find("_weaponManager").GetComponent<weaponManager>().weapons;
				foreach(GameObject weapon in listOfWeaponsR)
				{
					weapon com = weapon.GetComponent<weapon>();
					if((com.hand.ToString() == "BOTH" || com.hand.ToString() == "RIGHT"))
					{
					dynamicItemList.Add(new PanelItems() {
							name = com.weaponName,
							nextPanel = nextPanel,
							displayModel = com.menuModel,
							callFunction = "selectRightWeapon"
						});
					}
				}
			break;
			case DynamicItems.LeftWeapon:
				var listOfWeaponsL = GameObject.Find("_weaponManager").GetComponent<weaponManager>().weapons;
				foreach(GameObject weapon in listOfWeaponsL)
				{
					weapon com = weapon.GetComponent<weapon>();
					if((com.hand.ToString() == "BOTH" || com.hand.ToString() == "LEFT"))
					{
					dynamicItemList.Add(new PanelItems() {
							name = com.weaponName,
							nextPanel = nextPanel,
							displayModel = com.menuModel,
							callFunction = "selectLeftWeapon"
						});
					}
				}
			break;
			case DynamicItems.Review:

			break;
			case DynamicItems.Resolution:
				Resolution[] resolutions = Screen.resolutions;
	            for (int i = 0; i < resolutions.Length; i++) {
					dynamicItemList.Add(new PanelItems() {
						name = resolutions[i].width + "x" + resolutions[i].height + " @" + resolutions[i].refreshRate + "Hz",
						nextPanel = nextPanel,
	                    callFunction = "ChangeResolution",
						optionalString = i.ToString()
	                });
	            }
	            // Screen.SetResolution (resolutions[0].width, resolutions[0].height, true);
	            // Screen.currentResolution.width
	            // Screen.currentResolution.height
	            // Screen.currentResolution.refreshRate
			break;
			case DynamicItems.Quality:
				string[] names  = QualitySettings.names;
				for (int i = 0; i < names.Length; i++) {
					dynamicItemList.Add(new PanelItems() {
						name = names[i],
						nextPanel = nextPanel,
						callFunction = "ChangeQuality",
						optionalString = i.ToString()
					});
				}
			break;
		}

		
		
		if(items != null && items.Count > 0){
//			int i = 0;
			foreach(PanelItems item in items){
				dynamicItemList.Add(item);
//				item.background = background;
//				item.hover = hover;
//				item.pressed = pressed;
//				item.fontSize = fontSize;
//				item.width = buttonWidth;
//				item.padding = ((padding + fontSize) / 2) * 0.01f;
//				addText(item, i);
//				i++;
			}
		}
		
		if(dynamicItemList != null && dynamicItemList.Count > 0){
			int i = 0;
			foreach(PanelItems item in dynamicItemList){
				item.background = background;
				item.hover = hover;
				item.pressed = pressed;
				item.fontSize = fontSize;
				item.width = buttonWidth;
				item.padding = ((padding + fontSize) / 2) * 0.01f;
				addText(item, i);
				i++;
			}
		}




		/**
		 * Start with the first item selected.
		 *
		 * */
		FocusButton(0);


		/**
		 * Add background to panel
		 *
		 */
		if(showPanelBackground)
		{
			GameObject plane = new GameObject("menu_background");
			plane.transform.parent = transform;
			plane.transform.position = transform.position;
			plane.transform.rotation = new Quaternion (0,90,0,0);

			//var mDistance = Vector3.Distance(Cameras.transform.position, transform.position);

			//var width  = (Screen.width * 0.0001163636f) * (Screen.height * (0.001203636f * Screen.width / Screen.height));//0.128f;//((Screen.width  * 0.002f) * (backgroundSize.x * 0.01f));// * mDistance;
			//var height = 0.063f;//Screen.height * 0.00014179104f;//0.056f;//((Screen.height * 0.002f) * (backgroundSize.y * 0.01f));
			var width  = backgroundSize.x * 0.01f;
			var height = backgroundSize.y * 0.01f;

			MeshFilter meshFilter = (MeshFilter)plane.AddComponent(typeof(MeshFilter));
			meshFilter.mesh = CreateMesh(width, height);
			MeshRenderer renderer = plane.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
			renderer.material.shader = Shader.Find ("Unlit/Transparent" );
			Texture2D tex = new Texture2D(1, 1);
			tex.SetPixel(0, 0, panelBackground);
			tex.Apply();
			renderer.material.mainTexture = tex;
			renderer.material.color = panelBackground;
		}
	}

	void Update ()
	{
		//Debug.Log ("W"+Screen.height + " 0.057 " + Screen.height * a);

		if(InputManager.upDown)
		{
			if(buttonList == null)
				return;

			if(buttonFocus == 0)
			{
				FocusButton(buttonList.Count-1);
			}
			else
			{
				FocusButton(buttonFocus-1);
			}
		}

		if(InputManager.downDown)
		{
			if(buttonList == null)
				return;

			if(buttonFocus+1 == buttonList.Count)
			{
				FocusButton(0);
			}
			else
			{
				FocusButton(buttonFocus+1);
			}
		}

		if (buttonList != null && InputManager.submitDown && buttonList.Count > 0)
		{
			buttonList[buttonFocus].gameObject.BroadcastMessage("Pressed"); //.GetComponentInChildren<menuButtonHover>().Pressed();
		}
		else if (buttonList != null && InputManager.submitUp && buttonList.Count > 0)
		{
			buttonList[buttonFocus].gameObject.BroadcastMessage("Release");// GetComponentInChildren<menuButtonHover>().Release();
		}

		if (InputManager.cancelDown && prevPanel != null && (GameManager.Instance.gameState != GameStates.Calibrate && GameManager.Instance.gameState != GameStates.Recalibrate))
		{
			prevPanel.SetActive(true);
			gameObject.SetActive(false);
		}
	}

	public void ButtonPressed(int index)
	{
//		if(items != null && items[index].callFunction != null && items[index].callFunction != ""){
//			MenuManager.Instance.SendMessage(items[index].callFunction, items[index], SendMessageOptions.DontRequireReceiver);
//			//SendMessageUpwards(items[index].callFunction, items[index], SendMessageOptions.DontRequireReceiver);
//		}
	}

	public void ButtonReleased(int index)
	{
		//Debug.Log("ButtonReleased");
		if(dynamicItemList != null && dynamicItemList[index].callFunction != null && dynamicItemList[index].callFunction != ""){
			MenuManager.Instance.SendMessage(dynamicItemList[index].callFunction, dynamicItemList[index], SendMessageOptions.DontRequireReceiver);
			//SendMessageUpwards(dynamicItemList[index].callFunction, dynamicItemList[index], SendMessageOptions.DontRequireReceiver);
		}

		if(dynamicItemList != null && dynamicItemList[index].nextPanel != null){
			//Debug.Log(dynamicItemList[index].nextPanel);
			dynamicItemList[index].nextPanel.SetActive(true);
			gameObject.SetActive(false);
		}
	}




	/// <summary>
	/// The following methods are actions to be taken on behalf of events captured from user input.
	/// </summary>
	public void FocusButton(int index)
	{
		if(buttonList == null || buttonList.Count == 0)
			return;
		
		buttonList[buttonFocus].gameObject.GetComponentInChildren<MenuButton>().Blur();

		if(scrolling && buttonList.Count > 1)
		{
			if(index > 1 && index <= (buttonList.Count-1))
			{
				var currPos = startPos + new Vector3(0,(fontSize / 160f) * (index - 2),0);
				thisTransform.localPosition = currPos;
				for(int i = 0; i < buttonList.Count; i++)
				{
					int lead = 3;

					if(i == index || (i < index + lead && i > index - 3))
					{
						buttonList[i].SetActive(true);
					}
					else
					{
						buttonList[i].SetActive(false);
					}
				}
			}
			else
			{
				// This affects the first two in the menu
				var currPos = startPos + new Vector3(0,0,0);
				thisTransform.localPosition = currPos;
				for(int i = 0; i < buttonList.Count; i++)
				{
					int lead = 5;
					if(index == 1)
						lead = 4;
					if(index == 2)
						lead = 3;
					if(i == index || i < index + lead)
					{
						buttonList[i].SetActive(true);
					}
					else
					{
						buttonList[i].SetActive(false);
					}
				}
			}
		}

		buttonList[index].gameObject.GetComponentInChildren<MenuButton>().Focus();
		buttonFocus = index;

		if(dynamicItems == DynamicItems.RightWeapon || dynamicItems == DynamicItems.LeftWeapon)
		{
			showWeapon(index);
		}


	}

	void showWeapon(int index){
		var sword = GameObject.Find("menu_tempObj");
		if(sword != null)
			Destroy(sword.gameObject);

		if(dynamicItemList[index].displayModel == null)
			return;

        GameObject tempObj = new GameObject("menu_tempObj");
		tempObj.transform.parent = transform;
		GameObject displayWeapon = Instantiate(dynamicItemList[index].displayModel) as GameObject;
		displayWeapon.transform.parent = tempObj.transform;
		displayWeapon.transform.position = transform.position + new Vector3(-0.1f,-0.4f,-2);
		displayWeapon.transform.localScale = new Vector3(2,2,2);
		displayWeapon.AddComponent<continuousRotate>();
	}

	void addText(PanelItems item, int multiplier)
	{
		//Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
		//ArialFont.material.shader = Shader.Find ("GUI/3D Text Shader");

        GameObject menuButton = new GameObject("menu_" + item.name);

		//menuButton.AddComponent<menuButton>(); // This was an empty class, I renamed menuButtonHover to menuButton as it was more fitting.

		menuButton.transform.parent = transform;
		menuButton.transform.position = transform.position;
        menuButton.transform.localPosition = new Vector3(0f, -(item.padding * multiplier), 0);
		menuButton.transform.localScale = new Vector3 (0.04f,0.04f,0.04f);


        TextMesh textMeshComponent = menuButton.AddComponent<TextMesh>();
        textMeshComponent.font = font;
        textMeshComponent.fontSize = item.fontSize;
        textMeshComponent.text = item.name;
        menuButton.renderer.material = fontMaterial;//font.material;


		GameObject plane = new GameObject("menu_" + item.name + "_background");

	    MeshFilter meshFilter = (MeshFilter)plane.AddComponent(typeof(MeshFilter));
	    meshFilter.mesh = CreateMesh(item.width, item.fontSize / 17f);
	    MeshRenderer renderer = plane.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
	    renderer.material.shader = Shader.Find ("Custom/Transparent Color" );
	    Texture2D tex = new Texture2D(1, 1);
	    tex.SetPixel(0, 0, item.background);
	    tex.Apply();
	    renderer.material.mainTexture = tex;
	    renderer.material.color = item.background;

		plane.transform.parent = menuButton.transform;
		plane.transform.position = menuButton.transform.position;
		plane.transform.localRotation = new Quaternion(0,180,0,0);
        plane.transform.localPosition = new Vector3(item.width - 1, -(item.fontSize / 16f), 0.01f);
		plane.transform.localScale = new Vector3 (1f,1f,1f);
		plane.AddComponent<BoxCollider>();


		MenuButton hoverComponent = plane.AddComponent<MenuButton>(); // menuButton was an empty class, I renamed menuButtonHover to menuButton as it was more fitting.

		hoverComponent.btn = menuButton;
		hoverComponent.hover = hover;
		hoverComponent.pressed = pressed;
		hoverComponent.background = background;
		hoverComponent.index = multiplier;
		hoverComponent.menuPanel = this;

		buttonList.Add(menuButton);
	}

	Mesh CreateMesh(float width, float height)
	{
	    Mesh m = new Mesh();
	    m.name = "ScriptedMesh";
	    m.vertices = new Vector3[] {
	       new Vector3(-width, -height, 0.01f),
	       new Vector3(width, -height, 0.01f),
	       new Vector3(width, height, 0.01f),
	       new Vector3(-width, height, 0.01f)
	    };
	    m.uv = new Vector2[] {
	       new Vector2 (0, 0),
	       new Vector2 (0, 1),
	       new Vector2(1, 1),
	       new Vector2 (1, 0)
	    };
	    m.triangles = new int[] { 0, 1, 2, 0, 2, 3};
	    m.RecalculateNormals();

	    return m;
	}
}
