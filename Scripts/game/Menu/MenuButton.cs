using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour {
	
	public int index = 0;
	public Color background;
	public Color hover;
	public Color pressed;
	public GameObject btn;
	public MenuPanel menuPanel;

	private Texture2D tex;
	private bool hasLeft = true;
	
	void OnEnable(){
		tex = new Texture2D(1, 1);
	}
	
	public void Focus(){
		hasLeft = false;
	    tex.SetPixel(0, 0, hover);
	    tex.Apply();
	    renderer.material.mainTexture = tex;
	    renderer.material.color = hover;

	}
	
	public void Blur(){
		hasLeft = true;
	    tex.SetPixel(0, 0, background);
	    tex.Apply();
	    renderer.material.mainTexture = tex;
	    renderer.material.color = background;
	}
	
	public void Pressed(){
		if(hasLeft == true) 
			return;
		
	    tex.SetPixel(0, 0, pressed);
	    tex.Apply();
	    renderer.material.mainTexture = tex;
		renderer.material.color = pressed;
		menuPanel.ButtonPressed(index);
		//SendMessageUpwards("ButtonPressed", index, SendMessageOptions.RequireReceiver);
	}
	
	public void Release(){
		if(hasLeft == false){
			OnMouseEnter();
			menuPanel.ButtonReleased(index);
			//SendMessageUpwards("ButtonReleased", index, SendMessageOptions.RequireReceiver);
		}
	}
	
	public void OnMouseEnter(){
		menuPanel.FocusButton(index);
		//SendMessageUpwards("Focus", index, SendMessageOptions.RequireReceiver);
	}
	
	public void OnMouseExit(){
		Blur();
	}
	
//	public void OnMouseDown(){
//		Pressed();
//	}
	
//	public void OnMouseUp(){
//		Release();
//	}
}
