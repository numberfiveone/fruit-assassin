﻿using UnityEngine;
using System.Collections;

public class CollisionSplash : MonoBehaviour {

	void OnCollisionEnter(Collision other) {
		if(other.gameObject.name.IndexOf("(Clone)(Clone)") == -1 && other.gameObject.tag == "fruit"){
			BleedBehavior.BloodAmount = 1;
		}
	}
}
