using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MenuManager : MonoBehaviour {
	// Singleton  
	private static MenuManager instance;   
	
	// Construct  
	//private Clicker() {}    
	
	//  Instance  
	public static MenuManager Instance  
	{     
		get     
		{       
			if (instance ==  null)
				instance = GameObject.FindObjectOfType(typeof(MenuManager)) as  MenuManager;      
			return instance;    
		}   
	}

//	void Update()
//	{
//
//	}

	public void StartGamePref()
    {
        StartGame();
    }

	public void UnpausePref ()
	{
		GameManager.Instance.gameState = GameStates.Unpaused;
	}
	
	public void RecallibratePref ()
	{
		GameManager.Instance.gameState = GameStates.Recalibrate;
		GameObject.Find("Left Hand").GetComponent<SixenseHandController>().EnableHand();
		GameObject.Find("Right Hand").GetComponent<SixenseHandController>().EnableHand();
	}
	
	public void LeaveGamePref ()
	{
		GameManager.Instance.gameState = GameStates.Startup;
		Application.LoadLevel("menu-test");
	}

    public void StartGame(string scene = null)
    {
        string sceneToLoad = null;
        if(scene != null)
            sceneToLoad = scene;
        else
            sceneToLoad = PlayerPrefs.GetString("sceneName");

        Application.LoadLevel(sceneToLoad);
    }

    public void ResetOculusOrientation ()
    {
        if((OVRDevice.IsHMDPresent() == true) && (OVRDevice.IsSensorPresent(0) == true))
        {
            OVRDevice.ResetOrientation(0);
        }
    }

    public void selectMap(PanelItems index)
    {
        if(index.optionalString != null && index.optionalString != ""){
            PlayerPrefs.DeleteKey("sceneName");
            PlayerPrefs.DeleteKey("mapName");
            PlayerPrefs.SetString("sceneName", index.optionalString);
            PlayerPrefs.SetString("mapName", index.name);
        }
    }

    public void selectRightWeapon(PanelItems index)
    {
        PlayerPrefs.DeleteKey("rightWeapon");
        PlayerPrefs.SetString("rightWeapon", index.name);
    }

    public void selectLeftWeapon(PanelItems index)
    {
        PlayerPrefs.DeleteKey("leftWeapon");
        PlayerPrefs.SetString("leftWeapon", index.name);
    }

    public void ChangeResolution (PanelItems index)
    {
        if(index.optionalString != null)
        {
	        int i;
	        int.TryParse(index.optionalString, out i);
			Resolution[] resolutions = Screen.resolutions;

            try
            {
                var newRes = resolutions[i];
                Screen.SetResolution (newRes.width, newRes.height, true);
            }
            catch{}
        }
    }
	
	public void ChangeQuality (PanelItems index)
	{
		if(index.optionalString != null)
		{
			int i;
			int.TryParse(index.optionalString, out i);
			try
			{
				QualitySettings.SetQualityLevel(i, false);
			}
			catch{}
		}
	}

    public static void ToggleFullScreen ()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }

    public void QuitGame ()
    {
        Application.Quit();
    }
}