using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class InputManager : MonoBehaviour {
	// Singleton  
	private  static InputManager instance;   
	
	// Construct  
	//private Clicker() {}    
	
	//  Instance  
	public static InputManager Instance  
	{     
		get     
		{       
			if (instance ==  null)
				instance = GameObject.FindObjectOfType(typeof(InputManager)) as  InputManager;      
			return instance;    
		}   
	}

	private bool bUseHydras = true;
//	private bool UseXbox = true;
//	private bool UseMouse = true;
//	private bool UseKeyboard = true;


	[HideInInspector]
	public SixenseInput.Controller controllerR; // get right controller
	[HideInInspector]
	public SixenseInput.Controller controllerL; // get left controller
	
	[HideInInspector]
	public string verticalAxisName = "Vertical";

	[HideInInspector]
	public KeyCode submitKey0 = KeyCode.Return;
	[HideInInspector]
	public KeyCode submitKey1 = KeyCode.JoystickButton0;
	[HideInInspector]
	public KeyCode submitKey2 = KeyCode.Mouse0;
	[HideInInspector]
	public KeyCode cancelKey0 = KeyCode.Escape;
	[HideInInspector]
	public KeyCode cancelKey1 = KeyCode.JoystickButton1;

	[HideInInspector]
	public bool submitButtonDown = false;
	[HideInInspector]
	public bool submitButtonUp = false;

	[HideInInspector]
	public bool cancelButtonDown = false;
	[HideInInspector]
	public bool cancelButtonUp = false;

	[HideInInspector]
	public bool startButtonDown = false;

	[HideInInspector]
	public static bool cancelDown = false;
	[HideInInspector]
	public static bool cancelUp = false;

	[HideInInspector]
	public static bool submitDown = false;
	[HideInInspector]
	public static bool submitUp = false;

	[HideInInspector]
	public static bool upDown = false;
	[HideInInspector]
	public static bool upUp = false;

	[HideInInspector]
	public static bool downDown = false;
	[HideInInspector]
	public static bool downUp = false;

	[HideInInspector]
	public static bool startDown = false;
	

	// Used to ensure that joystick-based controls don't trigger that often
	static float mNextEvent = 0f;

	void Update ()
	{
		int vertical = 0;

		if(bUseHydras)
			hydraInput();

		if (!string.IsNullOrEmpty(verticalAxisName))
			vertical = GetDirection(verticalAxisName);
		
        submitDown = submitButtonDown || (submitKey0 != KeyCode.None && Input.GetKeyDown(submitKey0)) || (submitKey1 != KeyCode.None && Input.GetKeyDown(submitKey1)) || (submitKey2 != KeyCode.None && Input.GetKeyDown(submitKey2));
        submitUp   = submitButtonUp || (submitKey0 != KeyCode.None && Input.GetKeyUp(submitKey0)) || (submitKey1 != KeyCode.None && Input.GetKeyUp(submitKey1)) || (submitKey2 != KeyCode.None && Input.GetKeyUp(submitKey2));

        cancelDown = cancelButtonDown || (cancelKey0 != KeyCode.None && Input.GetKeyDown(cancelKey0)) || (cancelKey1 != KeyCode.None && Input.GetKeyDown(cancelKey1));
		
		upDown = (Input.GetKeyDown(KeyCode.UpArrow) || vertical > 0);
		downDown = (Input.GetKeyDown(KeyCode.DownArrow) || vertical < 0);
		
		startDown = startButtonDown;



		if(Input.GetKeyDown(KeyCode.F11))
		{
			MenuManager.ToggleFullScreen();
		}

		if(startDown || (cancelKey0 != KeyCode.None && Input.GetKeyDown(cancelKey0)))
		{
			switch(GameManager.Instance.gameState){
				case GameStates.Paused:
					GameManager.Instance.gameState = GameStates.Unpaused;
				break;
				case GameStates.Playing:
					GameManager.Instance.gameState = GameStates.Paused;
				break;
			}
		}

		if(cancelDown){
			switch(GameManager.Instance.gameState){
				case GameStates.Calibrate:
				case GameStates.GameOver:
					GameManager.Instance.LoadMainMenu();
				break;
			}
		}
		
		if(GameManager.Instance.gameState == GameStates.Ready)
		{
			if(submitUp)
			{
				GameManager.Instance.gameState = GameStates.CountDown;
			}
		}
		
		if(GameManager.Instance.gameState == GameStates.GameOver)
		{
			if(submitUp)
			{
				GameManager.Instance.StartLevel();
				GameManager.Instance.gameState = GameStates.Calibrate;
			}
		}
	}

	void hydraInput(){
        controllerR = SixenseInput.GetController (SixenseHands.RIGHT); // get right controller
        controllerL = SixenseInput.GetController (SixenseHands.LEFT); // get left controller

        if(controllerR != null && controllerL != null)
        {
            submitButtonDown = controllerR.GetButtonDown(SixenseButtons.ONE) || controllerL.GetButtonDown(SixenseButtons.ONE);
            submitButtonUp = controllerR.GetButtonUp(SixenseButtons.ONE) || controllerL.GetButtonUp(SixenseButtons.ONE);
            cancelButtonDown = controllerR.GetButtonDown(SixenseButtons.TWO) || controllerL.GetButtonDown(SixenseButtons.TWO);
            cancelButtonUp = controllerR.GetButtonUp(SixenseButtons.TWO) || controllerL.GetButtonUp(SixenseButtons.TWO);
            startButtonDown = controllerR.GetButtonDown(SixenseButtons.START) || controllerL.GetButtonDown(SixenseButtons.START);
        }
    }


    /// <summary>
    /// Using the joystick to move the UI results in 1 or -1 if the threshold has been passed, mimicking up/down keys.
    /// </summary>

    public int GetDirection (string axis)
    {
        float time = Time.realtimeSinceStartup;
        if (mNextEvent < time)
        {
            float val = 0;
            if (controllerL != null && controllerL.Enabled )
            {
                if(axis == "Vertical")
                    val +=  controllerL.JoystickY;
                else if(axis == "Horizontal")
                    val +=  controllerL.JoystickX;

            }
            if (controllerR != null && controllerR.Enabled )
            {
                if(axis == "Vertical")
                    val +=  controllerR.JoystickY;
                else if(axis == "Horizontal")
                    val +=  controllerR.JoystickX;
            }
            if(controllerR == null && controllerL == null)
            {
                val = Input.GetAxis(axis);
            }
            if (val > 0.75f)
			{
				mNextEvent = time + 0.25f;
                return 1;
            }
            if (val < -0.75f)
			{
				mNextEvent = time + 0.25f;
                return -1;
            }
        }
        return 0;
    }
}