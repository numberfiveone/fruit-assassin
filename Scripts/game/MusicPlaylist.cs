﻿using UnityEngine;
using System.Collections;

public class MusicPlaylist : MonoBehaviour {
	// Singleton  
	private  static MusicPlaylist instance;   
	
	// Construct  
	//private Clicker() {}    
	
	//  Instance  
	public static MusicPlaylist Instance  
	{     
		get     
		{       
			if (instance ==  null)
				instance = GameObject.FindObjectOfType(typeof(MusicPlaylist)) as  MusicPlaylist;      
			return instance;    
		}   
	}
	
	public AudioClip [] music;
	public bool random = false;
	public bool autoStart = false;
	
	private int currentTrack = 1;
	
	// Use this for initialization
	void Start () {
		if(autoStart && !random)
		{
			audio.clip = music[Random.Range(1,music.Length)];
		    audio.Play();
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(autoStart && !audio.isPlaying && GameManager.Instance.gameState == GameStates.Playing){
        	playNext();
			audio.volume = 0.25f;
		}
	}

	public void PlayTrack(int track){
		audio.Stop();
		audio.clip = music[track];
		audio.Play();
	}
	
	public void playNext ()
	{
		audio.Stop();
		int totalTracks = music.Length - 1;
		
		if(totalTracks < 2)
			return;
		
		if(random)
		{
			currentTrack = Random.Range(1,totalTracks);
		}
		else
		{
			if(currentTrack >= totalTracks)
				currentTrack = 1;
			else
				currentTrack += 1; 
			
		}
	    audio.clip = music[currentTrack];
	    audio.Play();
	}
}
