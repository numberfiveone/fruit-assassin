//
// Copyright (C) 2013 Sixense Entertainment Inc.
// All Rights Reserved
//

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SixenseHandController : SixenseObjectController
{
	private Vector3 startPos;
	private bool bHandIsVisibable = true;
	protected override void Start() 
	{
		startPos = transform.position;
		base.Start();
	}
	
	protected override void UpdateObject( SixenseInput.Controller controller )
	{
		if (controller.Enabled && controller.GetButtonDown(SixenseButtons.TRIGGER))
		{
	        /**
	         *  Calls SixenseObjectController:EnableHand()
	         *  This class is extended off that.
	         */
	        EnableHand ();
		}

		if(m_enabled && !bHandIsVisibable)
		{
			showHand();
			bHandIsVisibable = true;
		}
		else if(!m_enabled && bHandIsVisibable)
		{
			hideHand ();
			bHandIsVisibable = false;
		}
		
		base.UpdateObject( controller );
	}

	public void hideHand ()
	{
		transform.position = new Vector3(0,-10,0);
	}
	
	public void showHand ()
	{
		transform.position = startPos;
	}
} 