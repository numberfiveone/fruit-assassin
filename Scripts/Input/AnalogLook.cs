﻿using UnityEngine;
using System.Collections;

public class AnalogLook : MonoBehaviour {

	public float maximumX = 360F;
	public float maximumY = 60F;
	public float sensitivity = 0.05f;

	float rotationX = 0F;
	float rotationY = 0F;
	private float newRotationX = 0f;
	private float newRotationY = 0f;
	private Quaternion originalRotation;
	
	void Start ()
	{
		originalRotation = transform.localRotation;
	}

	void Update ()
	{
		if(GameManager.Instance.gameState == GameStates.Playing)
		{
			SixenseInput.Controller hydraController = SixenseInput.GetController (SixenseHands.RIGHT);
			float forwardInput = 0f;
			float turnInput = 0f;
			
			if(hydraController != null && hydraController.Enabled && !hydraController.Docked)
			{
				forwardInput = hydraController.JoystickY;
				turnInput = hydraController.JoystickX;
			}
			rotationX = turnInput * maximumX;//ClampAngle(rotationX, minimumX, maximumX);
			rotationY = forwardInput * maximumY;//ClampAngle(rotationY, minimumY, maximumY);
			
			newRotationX = Mathf.Lerp(newRotationX, rotationX, sensitivity * Time.deltaTime);
			newRotationY = Mathf.Lerp(newRotationY, rotationY, sensitivity * Time.deltaTime);
		}
		else
		{
			rotationX = 0f;
			rotationY = 0f;
			
			newRotationX = Mathf.Lerp(newRotationX, rotationX, sensitivity * Time.fixedDeltaTime);
			newRotationY = Mathf.Lerp(newRotationY, rotationY, sensitivity * Time.fixedDeltaTime);
		}

        Quaternion xQuaternion = Quaternion.AngleAxis(newRotationX, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis(newRotationY, Vector3.left);

        transform.localRotation = originalRotation * xQuaternion * yQuaternion;

	}
}
