﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class weapon : MonoBehaviour {
	public string weaponName;
	public GameObject menuModel;
	public Hands hand;
	public availableMaps[] usableOnMaps;
	public GameObject[] fx;

	public void DoBonus(string Bonus){

		fx[0].SetActive(true);
		for(int i = 0; i<fx.Length; i++)
		{
			fx[i].GetComponent<TCParticleSystem>().EmissionRate = 35000;
		}
	}

	public IEnumerator StopBonus(string Bonus){
		for(int i = 0; i<fx.Length; i++)
		{
			fx[i].GetComponent<TCParticleSystem>().EmissionRate = 0;
		}
		yield return new WaitForSeconds(10);
		fx[0].SetActive(false);

	}
}