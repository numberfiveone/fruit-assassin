﻿using UnityEngine;
using System.Collections;

public class weaponSelector : MonoBehaviour {
	
	private string rightWeapon;
	private string leftWeapon;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	public void selectWeapon () 
	{
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		Debug.DrawRay (transform.position, fwd, Color.green);
		
        RaycastHit hit;
        if (Physics.Raycast(transform.position, fwd, out hit, 10))
		{
			string objectName = hit.transform.gameObject.name.ToString();
			if(objectName.IndexOf("weapon") > -1)
			{
				Vector3 p = GameObject.Find("right_light").transform.position;
				p.x = hit.transform.position.x;
				GameObject.Find("right_light").transform.position = p;
				var weaponName = objectName.Split('_')[0];
				PlayerPrefs.SetString("selectedWeapon", weaponName);
		 		print(	weaponName );
			}
				
		}
	}
}
