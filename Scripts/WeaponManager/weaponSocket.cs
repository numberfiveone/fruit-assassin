﻿using UnityEngine;
using System.Collections;
public class weaponSocket : MonoBehaviour {
	
	private string useWeapon; // Default fallback
	public Hands hand;
	
	
	void Start()
	{
		var weaponList = GameObject.Find("_weaponManager").GetComponent<weaponManager>().weapons;

		
		if(hand.ToString() == "LEFT")
		{
			useWeapon = PlayerPrefs.GetString("leftWeapon");
		}
		else if(hand.ToString() == "RIGHT")
		{
			useWeapon = PlayerPrefs.GetString("rightWeapon");
		}

		
		foreach(GameObject selectedWeapon in weaponList)
		{
			var com = selectedWeapon.GetComponent<weapon>();
			if(com.weaponName == useWeapon && (com.hand.ToString() == "BOTH" || com.hand.ToString() == hand.ToString()))
			{
				GameObject weapon = Instantiate (selectedWeapon, rigidbody.position, Quaternion.identity) as GameObject;
				ConfigurableJoint joint = transform.GetComponent<ConfigurableJoint>() as ConfigurableJoint;
				joint.connectedBody = weapon.rigidbody;
			}
		}
		GameManager.Instance.weaps = GameObject.FindGameObjectsWithTag("weapon");
	}
}
