﻿using UnityEngine;
using System.Collections;

public enum Hands
{
	LEFT = 0,
	RIGHT = 1,
	BOTH = 2,
}
public class weaponManager : MonoBehaviour {
	
	[HideInInspector]
	public Object[] weapons;
	
	public void Start (){
		weapons = Resources.LoadAll("Weapons");
	}
}
