﻿using UnityEngine;
using System.Collections;

public class QualityDisplay : MonoBehaviour {

	// Use this for initialization
	private TextMesh txt;
	private float nextTime;
	void Start () {
		nextTime = Time.realtimeSinceStartup;
		txt = transform.GetComponent<TextMesh>();
	}

	void Update () {
		if(nextTime < Time.realtimeSinceStartup){
			nextTime = Time.realtimeSinceStartup + 1;
			txt.text = "Current Quality Setting: " + QualitySettings.names[QualitySettings.GetQualityLevel()];
		}
	}
	

}
