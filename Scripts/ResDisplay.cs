﻿using UnityEngine;
using System.Collections;

public class ResDisplay : MonoBehaviour {

	// Use this for initialization
	private TextMesh txt;
	private float nextTime;
	void Start () {
		nextTime = Time.realtimeSinceStartup;
		txt = transform.GetComponent<TextMesh>();
	}
	
	void Update () {
		if(nextTime < Time.realtimeSinceStartup){
			nextTime = Time.realtimeSinceStartup + 1;

			txt.text = "Current Resolution: " + Screen.width + "x" + Screen.height;
		}
	}
}
