﻿//using UnityEngine;
//using System.Collections;
//
//public class MusicPlaylist : MonoBehaviour {
//
//	public AudioClip [] music;
//	public bool random = false;
//
//	private int currentTrack = 1;
//
//	// Use this for initialization
//	void Start () {
//		if(!random)
//		{
//			audio.clip = music[Random.Range(1,music.Length)];
//		    audio.Play();
//		}
//	}
//
//	// Update is called once per frame
//	void Update ()
//	{
//		if(!audio.isPlaying && gameObject.GetComponent<gameManager>().gameState == "playing")
//        	playNext();
//	}
//
//	public void playNext ()
//	{
//		audio.Stop();
//		int totalTracks = music.Length;
//
//		if(totalTracks < 2)
//			return;
//
//		if(random)
//		{
//			currentTrack = Random.Range(1,totalTracks);
//		}
//		else
//		{
//			if(currentTrack >= totalTracks)
//				currentTrack = 1;
//			else
//				currentTrack += 1;
//
//		}
//	    audio.clip = music[currentTrack-1];
//	    audio.Play();
//	}
//}
